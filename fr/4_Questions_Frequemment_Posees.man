ManPagesTextFile
"4.Questions_Frequemment_Posees" "jeanphilippegoldman" 20190617 0
<entry> "Comment EasyAlign fait-il la macrosegmentation ?"
<normal> "L'algorithme interne calcule par lui-même des intervalles basés sur la durée du signal et la longueur des énoncés transcrits afin d'estimer la durée de chaque énoncé. Il s'appuie aussi sur les pauses."

<entry> "Comment EasyAlign fait-il la phonétisation ?"
<normal> "Le module grapheme-to-phoneme d'un système text-to-speech (du texte à la parole) complet, appelé eLite et développé à la Faculté Polytechnique de Mons en Belgique, effectue une analyse linguistique de la transcription orthographique pour produire une transcription phonétique fondée sur un dictionnaire phonétique et les règles de prononciation."
<entry> "Comment EasyAlign fait-il la segmentation?"
<normal> "Pour chaque énoncé, les transcriptions orthographique et phonétique sont utilisées par un programme de reconnaissance de la parole bien connu sous le nom de HTK (HMM Tool Kit) sur son mode de $$forced alignment$ (alignement forcé) afin de capturer les frontières temporelles des phonèmes et des mots."

<entry> "Comment EasyAlign fait-il la syllabation ?"
<normal> "La syllabation est fondée sur des règles. Celles-ci suivent deux principes : 1. il y a une et seulement une voyelle par syllabe et 2. Le principe de sonorité est utilisé afin de séparer les groupes de consonantiques. Les pauses sont aussi utilisées comme frontière syllabique."

<entry> "Qu'est-ce que l'alphabet phonétique SAMPA ?"
<normal> "SAMPA est un alphabet phonétique qui diffère légèrement de l'alphabet phonétique international (API). Il a été conçu pour l'utilisation par ordinateur et utilise des caractères simples. Ce tableau présente le SAMPA et l'API ensemble avec quelques exemples en français. Les différences entre le SAMPA et l'API sont écrites en gris."
<list_item> "\bu SAMPA pour le français :  https://www.phon.ucl.ac.uk/home/sampa/french.htm"
<list_item> "\bu SAMPA pour le protugais : https://www.phon.ucl.ac.uk/home/sampa/portug.htm"
<list_item> "\bu SAMPA pour l'espagnol : https://www.phon.ucl.ac.uk/home/sampa/spanish.htm"
<list_item> "\bu SAMPA pour le slovaque : "
???"
<entry> "Où sont les scripts d'EasyAlign ?"
<normal> "Le programme d'installation crée un dossier appelé plugin_easyalign dans le dossier Praat. Généralement, ce dossier est C:\Documents and Settings\<your name>\Praat\plugin_easyalign"

<entry> "Pourquoi certaines frontières de syllabes ne coïncident-elles pas avec les frontières des mots ?"
<normal> "La tier $syll est calculée à la dernière étape, après les tiers des phonèmes et des mots. La syllabation est faite via regroupement de phonèmes d'après les règles de sonorité. Les enchainements et les élisions peuvent créer des frontières syllabiques surprenantes. Par exemple :
%%des choses issues de radios commerciales ou de radios classiques et caetera%
[dɛ-ʃo-zi-sy-dʁa-djo-kɔ-mɛr-sja-lu-də-ʁa-djo-kla-si-kɛk-sɛ-tɛ-ʁa]"

<entry> "$$Out of memory$ lors de l'étape trois"
<normal> "L'étape trois commence par modifier la fréquence de l'échantillon de son à 16000 Hz pour l'ajuster à celle du modèle acoustique. Pour ce faire, Praat utilise un algorithme très précis mais utilisant beaucoup de mémoire. Cette erreur est souvent présente dans les fichiers son dépassant 10 minutes. Ce seuil peut etre influencé par l'odinateur. N'achetez pas un nouvel ordinateur Si cette erreur se produit, vous devriez scinder le fichier en plusieurs segments. Une autre solution serait de rééchantillonner le fichier à 16000 Hz via un autre programme de traitement de la parole tel que Audacity ou Goldwave."