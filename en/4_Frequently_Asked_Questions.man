ManPagesTextFile
"4.Frequently_Asked_Questions" "jeanphilippegoldman" 20190617 0
<entry> "How does EasyAlign do the macro-segmentation?"
<normal> "The internal algorithm computes a heuristics based on signal duration and utterance transcription length to estimate utterance duration and also relies on pauses."

<entry> "How do EasyAlign do the phonetization?"
<normal> "The grapheme-to-phoneme module of a full text-to-speech system named eLite and developed at Faculté Polytechnique de Mons, Belgium, does a linguistic analysis of the orthographic transcription to produce a phonetic transcription based on a phonetic dictionary and pronunciation rules."

<entry> "How does EasyAlign do the phone segmentation?"
<normal> "For each utterance, the orthographic and phonetic transcriptions are used by a well-known speech recognition engine named HTK (HMM Tool Kit) set to a “forced alignment” mode to catch the temporal boundaries of phones and words."

<entry> "How does EasyAlign do the syllab(ific)ation ?"
<normal> "It is rule-based. Two main principles are followed: 1. there is one and only one vowel per syllable. and 2. The sonority principle is used to split the consonant clusters. The pauses are also used as syllabic boundaries."

<entry> "What is the SAMPA phonetic alphabet?"
<normal> "SAMPA is a phonetic alphabet that slightly differs from the standardized International Phonetic Alphabet (IPA). It was designed for computer assessment and uses only simple characters. This table presents SAMPA and IPA together with some examples in French. The differences between SAMPA and IPA are shown in gray."
<list_item> "\bu SAMPA for French:  https://www.phon.ucl.ac.uk/home/sampa/french.htm"
<list_item> "\bu SAMPA for Portuguese: https://www.phon.ucl.ac.uk/home/sampa/portug.htm"
<list_item> "\bu SAMPA for Spanish: https://www.phon.ucl.ac.uk/home/sampa/spanish.htm"
<list_item> "\bu SAMPA for Slovak: 
???"
<entry> "Where are the EasyAlign scripts ?"
<normal> "The installer creates a folder named plugin_easyalign in the Praat folder. Generally, this folder is C:\Documents and Settings\<your name>\Praat\plugin_easyalign"

<entry> "Why don’t some syllables boundaries coincide with word bounaries ?"
<normal> "The $syll tier is computed in a last step, after phones and words tiers. The syllabation is done by grouping phones according to the sonority rule. Enchainements and elisions can make surprising syllabic boundaries. For example :
%%des choses issues de radios commerciales ou de radios classiques et caetera%
[dɛ-ʃo-zi-sy-dʁa-djo-kɔ-mɛr-sja-lu-də-ʁa-djo-kla-si-kɛk-sɛ-tɛ-ʁa]"

<entry> "$$Out of memory$ during step 3"
<normal> "The step 3 starts by modifying the sampling frequency of the sound at 16000 Hz to adjust it to the one of the acoustic models. For this, Praat use a very precise but memory-consuming algorithm. This error often occurs in sound files above 10 minutes. This threshold may depend on the computer. Do not buy a new computer. If this error occurs, you may split the file into several segments. Another solution is to resample the file at 16000 Hz with another speech software like Audacity or Goldwave."