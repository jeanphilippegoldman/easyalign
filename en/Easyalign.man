ManPagesTextFile
"EasyAlign" "jeanphilippegoldman" 20190617 0
<entry> "What is EasyAlign?"
<intro> "EasyAlign is a Praat plug-in. It produces a multi-tier annotation with a phones, syllables, words and utterances segmentation from a sound recording and the corresponding orthographic (or phonetic) transcription. The whole procedure consists of 3 automatic steps in between which some manual verification may be necessary. The 5 resulting tiers are grouped in a TextGrid and named as phones, syll, words, phono and ortho. EasyAlign works with data from 4 languages: French, Spanish, Brasilian Portuguese and Slovak."
<list_item> "@@1.Macro_Segmentation@"
<normal> "The first step creates a TextGrid with orthographic transcription."
<list_item> "@@2.Phonetization@"
<normal> "The second step converts an orthographic transcription (ortho) into a phonetic transcription (phono)."
<list_item> "@@3.Phone_Segmentation@"
<normal> "The third step segments and aligns the sound file to phones, syllables and words."
<list_item> "@@4.Frequently_Asked_Questions@"
<normal> "Find in-depth explanations about the rationale behind Easy Align and practical tips to enhance the whole alignment procedure."