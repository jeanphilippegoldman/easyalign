ManPagesTextFile
"1.Macrosegmentation" "jeanphilippegoldman" 20190617 0
<intro> "Tout d'abord, il faut créer un TextGrid avec la transcription orthographique du fichier son. Il existe trois options:"
<list_item> "Vous n'avez aucune transcription: Créez un nouveau TextGrid et réalisez une transcription (voir 1.1)."
<list_item> "Vous avez une transcription dans un format un-énoncé-par-ligne: Insérez votre transcription dans un TextGrid (voir 1.2)."
<list_item> "Vous avez une transcription divisée en paragraphes: Reformatez votre transcription dans un format un-énoncé-par-ligne (voir 1.3)."
<list_item> "Vous avez plus d'un locuteur dans votre fichier son ? Voir 1.4. Procédure pour un enregistrement avec plusieurs locuteurs."

<entry> "1.1. Créer un nouveau TextGrid et encoder une transcription"
<list_item> "##Chargez le fichier son# (menu Read/Read from file…) et selectionnez le bouton à droite ##Annotate/To TextGrid…#. Complétez les champs %%All tier names%: ortho et %%Point tiers%: laissez vide). Le nouveau TextGrid a le meme nom et la meme durée que le fichier son. Sa tier ortho est vide."
<list_item> "Ensuite, ##ouvrir le son et le TextGrid# ensemble (Sélectionner les deux et choisir le bouton à droite View & Edit), insérez les frontières des intervalles de préférence au milieu des pauses silencieuses et transcrivez le contenu des intervalles un par un, en les écoutant. Vous devrez peut-être écouter le même intervalle plusieurs fois en fonction de vos capacités de transcription. N'utilisez pas d'intervalles de plus de cinq secondes afin d'éviter une transcription fastidieuse. N'oubliez pas d'##enregistrer régulièrement votre TextGrid# (Menu Praat/Write/Write to text file…)"
<list_item> "Poursuivez la procédure à l'étape @@2.Phonetisation@"

<entry> "1.2. Insérer une transcription au format un-énoncé-par-ligne dans un TextGrid"
<list_item> "##Ouvrir un fichier son#: (Read\Read from file…) et la transcritpion orthographique correspondante préformatée au format un-énoncé-par-ligne. Prenez soin d'ouvrir le son en tant que Sound object (et non LongSound object) et la transcription en tant que Strings object (New\Read\Read Strings from raw text file…)."
<list_item> "Sélectionnez ces deux objets (Sound and Strings) et lancez le script ##1. Macrosegmentation# dans le sous-menu EasyAlign. Un objet TextGrid est créé. Il contient par defaut une seule tier appelée ortho."
<list_item> "Le son et le TextGrid sont ouverts ensemble pour une ##vérification manuelle#: vérifiez la position temporelle de chaque frontière en écoutant chaque énoncé un par un. Si la position d'alignement sur le temps est incorrecte, elle doit etre ajustée manuellement sur le silence entre deux énoncés."

<code> "Paramètres"
<code1> "Sélectionner un son et un TextGrid"
<code2> "##tier avec du texte#: nom du texte qui se trouvera dans la tier qui sera créée"
<code1> "Après la segmentation,"
<code2> "##ouvrir un son et un textgrid#: checkbox"

<list_item> "Ensuite, suivre la procédure normale depuis @@2.Phonetisation@."

<entry> "1.3. Reformater votre transcription dans un format un-énoncé-par-ligne"
<normal> "Dans le but d'avoir une transcription dans le format requis, insérer le caractère retour après chaque énoncé. Cette tâche de formatage peut être effectuée dans n'importe quel éditeur de texte." 

<normal> "Quelques astuces:"
<list_item> "\bu Les pauses sont de bons indices pour terminer énoncé."
<list_item> "\bu Essayez d'éviter les énoncés de plus de vingt mots."
<list_item> "\bu Supprimez les caractères ou les mots qui ne sont pas prononcés (tels les <html tags> ou autres signes muets). La ponctuation habituelle peut être gardée."
<list_item> "\bu Prenez soin de sauvegarder le fichier de la transcription dans un format Text pour qu'il soit lisible en tant que Strings par Praat."

<normal> "Dans l'exemple du %%Petit Prince%, le fichier texte se présente comme suit:"

<code1> "On ne connaît que les choses que l'on apprivoise, dit le renard."
<code1> "Les hommes n'ont plus le temps de rien connaître."
<code1> "Ils achètent des choses toutes faites chez les marchands."
<code1> "Mais comme il n'existe point de Marchands d'amis, les hommes n'ont plus d'amis."
<code1> "Si tu veux un ami, apprivoise-moi!"
<code1> "Adieu, dit le renard. Voici mon secret."
<code1> "Il est très simple: on ne voit bien qu'avec le coeur."
<code1> "L'essentiel est invisible pour les yeux."

<entry> "1.4. Procédure pour un enregistrement avec plusieurs locuteurs"
<normal> "Si un fichier son comporte plusieurs locuteurs, il faut différencier deux cas : 1. Il y a plusieurs locuteurs principaux; 2. Il y a un locuteur principal et les autres sont secondaires."

<list_item> "\bu Il y a plusieurs locuteurs principaux"
<list_item> "La production de chaque locuteur doit être transcrite et analysée dans un TextGrid séparé."
<list_item> "La procédure complète sera donc réitérée autant de fois qu'il y a de locuteurs principaux."
<list_item> "Il est possible d'afficher ces TextGrid ensemble. Pour cela, sélectionnez-les ainsi que le fichier son et utilisezla fonction Group en bas à droite."

<list_item> "\bu Il y a un locuteur principal et un locuteur secondaire"
<list_item> "Ajoutez deux tiers ($$Tier > Add interval tier$) que vous nommez $$loc2$ et $$event$."
<list_item> "La tier $$ortho$ contient la transcription orthographique du locuteur principal, la tier $$loc2$ contient les interventions du deuxième locuteur et la tier $$event$ est utilisée pour annoter les phénomènes paraverbaux."
<list_item> "Dans la tier $$event$ les phénomènes sont notés entre parenthèses : par exemple %%(rire)% ou %%(toux)%."


