ManPagesTextFile
"Template" "jeanphilippegoldman" 20190617 0
<intro> "intro the firs paragraph of a page, will look the same as normal but the search system may take acount of the distinction"
<entry> "entry headings in a larger police size"
<normal> "du blabla on peut faire
des retours à la ligne qui ne seront pas pris en compte."
<normal> "Voici un nouveau paragraphe."
<normal> "un mot en %italique"
<normal> "plusieurs %% mots en italique%"
<normal> "un mot en #gras"
<normal> "##plusieurs mots en gras#"
<normal> "une lettre en ^exposant"
<normal> "un lien vers une autre page @@EasyAlign@"
<list_item> "\bu list"
<list_item> "\bu list"
<code> "code"
<code1> "code1 du code indenté"
<code2> "code2 du code indenté deux fois"
