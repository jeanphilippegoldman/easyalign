ManPagesTextFile
"EasyAlign" "jeanphilippegoldman" 20191021 0
<entry> "Qu'est-ce qu'EasyAlign?"
<intro> "EasyAlign est un plug-in de l'application Praat. Il produit une annotation multi-tiers avec une segmentation en phonèmes, syllabes, mots et énoncés d'après l'enregistrement sonore et sa transcription orthographique (ou phonétique). La procédure complète consiste en trois étapes automatiques entre lesquelles une vérification manuelle peut être nécessaire. Les cinq tiers résultantes sont groupées dans un TextGrid et nommées: phones, syll, words, phono et ortho. EasyAlign fonctionne pour quatre langues : le francais, l'espagnol, le portugais brésilien et le slovaque."
<list_item> "@@1.Macrosegmentation@"
<normal> "La première étape crée un TextGrid avec la transcription orthographique."
<list_item> "@@2.Phonetisation@"
<normal> "La deuxième étape convertit la transcription orthographique (tier ortho) en une transcription phonétique (tier phono)."
<list_item> "@@3.Segmentation_en_phones@"
<normal> "La troisième étape segmente et aligne le fichier son en phonèmes, syllabes et mots."
<list_item> "@@4.Questions_Frequemment_Posees@"
<normal> "Vous trouverez les explications détaillées à propos de la logique de fonctionnement d'EasyAlign ainsi que des astuces pratiques pour améliorer toute la procédure d'alignement."
