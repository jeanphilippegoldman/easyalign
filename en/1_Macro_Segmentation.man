ManPagesTextFile
"1.Macro_Segmentation" "jeanphilippegoldman" 20190617 0
<intro> "First, you need to create a TextGrid with the orthographic transcription of your Sound file. Here are the 3 options:"
<list_item> "You don't have any transcription: Create a new TextGrid and make a new transcription (See 1.1)."
<list_item> "You have a transcription in a one-sentence-per-line format: Insert your transcription into a TextGrid (See 1.2)"
<list_item> "You have a transcription organized in paragraphs: Reformat your transcription into a one-sentence-per-line format (See 1.3)"
<list_item> "What if you have more than one speaker in your Sound file? See 1.4. Procedure for a multi-speaker recording"

<entry> "1.1. Create a new TextGrid and make your transcription"
<list_item> "##Load the Sound file# (menu Read/Read from file…) and select the right-hand button ##Annotate/To TextGrid…#. Fill the fields %%All tier names%: ortho and %%Point tiers%: leave empty). The new TextGrid has the same name and duration as the sound file and has an empty ortho tier."
<list_item> "Then, ##open the Sound and TextGrid# together (Select both and choose the right-hand button View & Edit), insert interval boundaries preferably in the middle of silent pauses and transcribe intervals one by one by listening to them. You might have to listen to the same interval several times depending on your transcription skills. Do not use intervals longer than 5 seconds to avoid fastidious transcription. Do not forget to ##regularly save your TextGrid# (Menu Praat/Write/Write to text file…)"
<list_item> "Follow the procedure from script @@2.Phonetization@."

<entry> "1.2. Insert a one-sentence-per-line transcription into a TextGrid"
<list_item> "##Open a sound file#: (Read\Read from file…) and its corresponding orthographic transcription which has been preformatted into a «one-sentence-per-line» format. Beware of opening the sound in Sound object (and not LongSound object) and the transcription as a Strings object (New\Read\Read Strings from raw text file…)."
<list_item> "Select these two objects (Sound and Strings) and launch the script ##1. Macro-segmentation# in the EasyAlign submenu. A TextGrid object is created. It contains a single tier named ortho by default."
<list_item> "The sound and the TextGrid are opened together for a ##manual checking#: check the temporal position of every boundaries by listening to each utterance one by one. If the time-aligned position is wrong, one should manually adjust it to to the silent pause between the two utterances."

<code> "Parameters"
<code1> "Select a Sound and a TextGrid"
<code2> "##text tier#: name of the text tier that will be created"
<code1> "After segmentation,"
<code2> "##open sound and textgrid#: checkbox"

<list_item> "Then follow the usual procedure from script @@2.Phonetization@."

<entry> "1.3. Reformat your transcription into a one-sentence-per-line format"
<normal> "In order to have the transcription in the required “one utterance by line” format, “return characters” must be inserted after each utterance. This formatting task can be done in any text editor." 

<normal> "Some tips:"
<list_item> "\bu Pauses are good triggers to end an utterance."
<list_item> "\bu Try to avoid utterances longer than 20 words."
<list_item> "\bu Delete characters or words that are not elicited (like <html tags> or other mute signs). The usual punctuation can be kept."
<list_item> "\bu Take care of saving your transcription file in Text format so it can be read as Strings by Praat."

<normal> "In the %%Petit Prince% example, the text file is as follows:"

<code1> "On ne connaît que les choses que l'on apprivoise, dit le renard."
<code1> "Les hommes n'ont plus le temps de rien connaître."
<code1> "Ils achètent des choses toutes faites chez les marchands."
<code1> "Mais comme il n'existe point de Marchands d'amis, les hommes n'ont plus d'amis."
<code1> "Si tu veux un ami, apprivoise-moi!"
<code1> "Adieu, dit le renard. Voici mon secret."
<code1> "Il est très simple: on ne voit bien qu'avec le coeur."
<code1> "L'essentiel est invisible pour les yeux."

<entry> "1.4. Procedure for a multi-speaker sound file"
<normal> "If a sound file has several speakers, two cases must be distinguished: 1. there are several main speakers; 2. there is one main speaker and the others are secondary."

<normal> "There are several main speakers :"
<list_item> "\bu The speech production of each speaker must be transcribed and analyzed in a separate TextGrid."
<list_item> "\bu The complete procedure will therefore be repeated as many times as there are main speakers."
<list_item> "\bu It is possible to display these TextGrid together. To do this, select them and the sound file and use the Group function at the bottom right."

<normal> "There is a main speaker and a secondary speaker (eg. speaker and interviewer)"
<list_item> "\bu Add two tiers (Tier > Add interval tier) that you name loc2 and event."
<list_item> "\bu The ortho tier contains the transcription of the main speaker, the loc2 tier contains the interventions of the secondary speaker and the tier event is used to annotate paraveral phenomena."

<normal> "In the tier event, the phenomena are noted in brackets: for example (laugh) or (cough)."


