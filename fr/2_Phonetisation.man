ManPagesTextFile
"2.Phonetisation" "jeanphilippegoldman" 20190617 0
<entry> "2.1. Phonétiser la transcription orthographique"
<intro> "La deuxième étape duplique la tier $$ortho$ en une tier $$phono$ (autrement dit avec les mêmes barrières) mais remplace la transcription orthographique par une transcription phonétique d'après l'alphabet phonétique SAMPA (Voir @@4.Questions_Frequemment_Posees@ pour comprendre comment cela est fait et ce qu'est le SAMPA)."

<code> "Paramètres"
<code1> "Sélectionner un ou plusieurs TextGrids"
<code2> "##tier ortho#: nom de la tier où se trouve la transcription orthographique"
<code2> "##tier phono#: nom de la nouvelle tier où sera créée la transcription phonétique"
<code1> "Si la tier phono existe," 
<code2> "Checkbox: sélectionnez si vous voulez l'écraser."
<code1> "##Après la phonétisation#: checkbox: ouvrir le TextGrid, afin de vérifier manuellement la phonétisation"

<entry> "2.2. Vérification manuelle"
<normal> "La transcription phonétique devrait maintenant être vérifiée pour tous les intervalles en les écoutant. Vous devez faire attention aux variations phonologiques telles que l'insertion ou la suppression de phonèmes ou encore les variantes de prononciation."
<normal> "Dans l'exemple de démonstration %%Le petit prince%, la seule différence entre la transcription phonétique automatique et la prononciation effective du locuteur se trouve dans l'énoncé 3 avec un schwa épenthetique à la fin du mot %%toutes%. Ajoutez manuellement ce schwa (dont le symbole est [\at]) et faites attention de ne pas ajouter ou supprimer de caractère espace."

<normal> "Dans le TextGrid, vérifiez et corrigez la tier phono. Voici une liste de ##causes possibles de variation#:"
<list_item> "\bu Erreurs de transcription (ex. %%il fait% vs. %%il a fait%)."
<list_item> "\bu Erreurs de prononciation (ex. %courgette [guRZEt])."
<list_item> "\bu Liaisons (eg. %%il est arrivé% [il E taRive] vs. [il E aRive]). Une étoile est ajoutée à certains phonèmes de liaison en français afin d'attirer l'attention sur eux: ex. [lEZ* ami]." 
<list_item> "\bu Elision du schwa %samedi [sam(\at)di] ou d'autres phonèmes %%il dit% [i(l)di]"
<list_item> "\bu Insertion de schwa (%%toutes faites% [tut\at fɛt])"
<list_item> "\bu Prononciation des nombres (%%dix-neuf cent cinquante% vs. %%mille neuf cent cinquante%)"
<list_item> "\bu Prononciation des acronymes (lire ou epeler, %ONU as [ɔɛny] vs. [ony])"
<entry> "2.3. Même nombre de mots ortho et de mots phono"
<normal> "Soyez attentif à garder les caractères espaces comme ils sont, afin de maintenir le même nombre de mots dans chaque intervalle des tiers $ortho et $phono. Un mot est une chaine de lettres séparées par un espace."
<normal> "Par exemple %%qu’avec le coeur% est phonetisé en [kavEk l\at k9R] (3 mots) ou %%qu’ avec le coeur% est phonetisé en [k avEk l\at k9R] (4 mots). Si vous deviez modifier le nombre de mots (chaine de lettres séparées par les espaces), l'étape suivante ne pourrait être réalisée."
