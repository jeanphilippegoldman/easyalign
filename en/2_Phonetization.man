ManPagesTextFile
"2.Phonetization" "jeanphilippegoldman" 20190617 0

<entry> "2.1. Phonetize the orthographic transcription"
<intro> "The second step duplicates the $$ortho tier$ into a $$phono tier$ (i.e. with the same boundaries) but replaces the orthographic transcription by a phonetic transcription according to the SAMPA phonetic alphabet (See Frequently Asked Questions section to see how it is done and what is SAMPA)."

<code> "Parameters"
<code1> "Select one or more TextGrids"
<code2> "##ortho tier#: name of the tier where the orthographic transcription can be found"
<code2> "##phono tier#: name of the new tier that will be created with the phonetization"
<code1> "If phono tier exists," 
<code2> "Checkbox: select wether you want to overwrite it or not."
<code1> "##After phonetization#: checkbox: open textgrid, in order to manually check the phonetization"

<entry> "2.2. Manual verification"
<normal> "The phonetic transcription should now be verified for every intervals by listening to them. You must pay attention to phonological variations like phoneme insertion or deletion or variants in pronunciations."
<normal> "In the demonstration example %%Le petit prince%, the unique difference between the automatic phonetic transcription and the speaker’s actual pronunciation lays in utterance 3 with an epenthetic schwa at the end of word %%toutes%. Add it manually this schwa (whose symbol is [\at]) and pay attention not  add or delete space characters."

<normal> "In the TextGrid, verify and correct the phono tier. Here is a list of ##common possible causes of variation#:"
<list_item> "\bu Transcription errors (eg. %%il fait% vs. %%il a fait%)."
<list_item> "\bu Pronunciation errors (eg. %courgette [guRZEt]."
<list_item> "\bu Liaisons (eg. %%il est arrivé% [il E taRive] vs. [il E aRive]). A star is added to some liaisons phones in French in order to draw attention on them: eg. [lEZ* ami]." 
<list_item> "\bu Elision of schwa %samedi [sam(\at)di] or of other phones %%il dit% [i(l)di]"
<list_item> "\bu Insertion of schwa (%%toutes faites% [tut\at fɛt])"
<list_item> "\bu Pronunciation of numbers (%%dix-neuf cent cinquante% vs. %%mille neuf cent cinquante%)"
<list_item> "\bu Pronunciation of acronyms (reading or spelling, %ONU as [ɔɛny] vs. [ony])"
<entry> "2.3. Same number of ortho words and phono words"
<normal> "Beware of keeping the space characters as they are, in order to maintain the same number of words in each interval of $ortho and $phono tier. A word is a string of letters separated by a space. An"
<normal> "For example %%qu’avec le coeur% is phonetized as [kavEk l\at k9R] (3 words) or %%qu’ avec le coeur% is phonetized as [k avEk l\at k9R] (4 words). Should you modifiy the number of words (strings of letters separated by spaces), the next step could not be performed."
