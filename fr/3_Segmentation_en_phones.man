ManPagesTextFile
"3.Segmentation_en_phones" "jeanphilippegoldman" 20190617 0
<normal> "La troisième étape a pour but de créer 3 nouvelles tiers alignées sur le signal: les tiers $phones, $syll et $words. Un programme de reconnaissance de la parole est utilisé afin d'estimer les frontières temporelles pour chaque mot et pour chaque phonème de ce mot. Ensuite, un ensemble de règles basées sur la sonorité phonologique définit les frontières syllabiques."
<normal>" Quand le TextGrid résultant et le fichier son sont ouverts ensemble, vous pouvez écouter les mots, les syllabes et les phonèmes individuellement et éventuellement ajuster manuellement les frontières."
<normal> "Dans le fichier exemple %%Le petit prince%, tous les énoncés sont alignés et segmentés correctement."
<entry> "3.1. Menu Segmentation: créer les tiers phones, syll et words"
<normal> "Voici la description du menu:"
<code> "Paramètres"
<code1> "Sélectionner un son et un TextGrid"
<code2> "##tier ortho#: nom de la tier où trouver la transcription orthographique"
<code2> "##tier phono#: nom de la tier où trouver la phonétisation manuellemnt vérifiée"
<code2> "Checkbox Overwrite: sélectionnez si vous souhaitez écraser les tiers existantes."

<code1> "Choisir une langue"
<code2> "##language#: #fra francais, #nan ??? #spa espagnol #slk slovaque #porbra portuguais brésilien"
<code2> "##Chars to ignore#: liste des caractères que vous voulez ignorer lors de la procédure de segmentation et d'alignement"
<code2> "Checkbox Precise endpointing ???: Les phrases (en ortho/phono) sont délimitées"
<code2> "1. approximativement (avec une seule barrière dans le milieu de la pause) - recommandé après la macrosegmentation automatique (étape 1.2)"
<code2> "2. précisement (avec deux barrières autour de la pause) - recommandé si l'étape trois a déjà été effectuée précédemment."

<code1> "Détection de la variation"
<code2> "##Checkbox Consider star#: utiliser cette option si vous n'avez pas vérifié manuellement la phonétisation automatique pour les liaisons"
<code2> "##Checkbox Allow elision#: utiliser cette option si vous n'avez pas vérifié manuellement la phonétisation automatique pour les suppressions de schwa"

<code1> "Après traitement," 
<code2> "Checkbox: Pre PTK thresholde: insérer la durée (en ms) du silence préocclusif"
<code2> "##Checkbox: Make syllable tier#: utiliser cette option afin de créer la tier $syll"
<code2> "##Checkbox: Open Sound and Textgrid#: utiliser cette option afin d'ouvrir le son et le Textgrid après traitement, pour vérification manuelle."

<entry> "3.2. Après traitement"
<normal> "Trois tiers sont ajoutées au TextGrid : phones, syll, words. Vérifiez que tous les énoncés sont bien alignés. A cette fin, il est possible de jeter un rapide coup d'oeil au son et au TextGrid ouverts ensemble et de chercher les énoncés mal alignés; ou de regarder le fenêtre Info qui imprime un résumé des résultats de l'étape trois. Plusieurs explications peuvent justifier un énoncés mal aligné. Corrigez le TextGrid d'après les differents cas décrits ci-dessus et ensuite lancer le script trois. Segmenter encore."

<list_item> "\bu ##Non-SAMPA characters in the phonetic transcription#: la transcription de la tier phono contient quelques caractères qui n'appartiennent pas à l'alphabet SAMPA. Afin de corriger cela, modifiez manuellement la tier phono ou relancez l'étape trois. Sachez que si l'étape 2.Phonétisation est lancée une nouvelle fois, les modifications manuelles de la tier phono seront écrasées."

<list_item> "\bu ##The number of words is different in phono and ortho tiers#: lors de la vérification manuelle de la transcription phonétique, un ou plusieurs caractères espace ont pu être insérés ou supprimés. Corrigez cela dans la tier phono et/ou ortho. Si l'erreur provient de la convertion automatique grapheme-to-phoneme (étape 2), corrigez-la et rapportez-la à l'auteur (jeanphilippegoldman at gmail.com)"

<list_item> "\bu ##The segmentation could not be done# soit parce que l'énoncé est trop long, soit parce que la transcription phonétique diffère trop de la prononciation, soit parce que la qualité du signal est trop faible, soit parce que le débit de parole est trop élevé. Voici quelques facons d'y remedier : (1) Vérifiez les transcriptions orthographiques et phonétiques des énoncés non alignés. Faites attention de ne pas relancer à cet étape le script #2.Phonétisation, ou les modifications de la transcription phonétique seraient écrasées.
- Divisez les utterances non-alignées en deux ou plus d'utterances en insérant des frontières dans les tiers phono et ortho. Assurez-vous que les frontières coincident."

