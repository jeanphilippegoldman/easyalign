ManPagesTextFile
"3.Phone_Segmentation" "jeanphilippegoldman" 20190617 0
<normal> "The third step aims at creating 3 new tiers aligned to the signal: the $phones, $syll and $words tiers. A speech recognition engine is used to estimate the temporal boundaries of each word and of each phoneme of that word. Then a set of phonological sonority-based rules defines the syllabic boundaries.
When the resulting TextGrid and the Sound file are opened together, you can listen to words, syllables and phones individually and eventually, make manual adjustments of boundaries.
In the example file %%Le petit prince%, all the utterances are aligned and segmented correctly."

<entry> "3.1. Menu Segmentation: create phones, syll and words tiers"
<normal> "Here is the description of the menu:"
<code> "Parameters"
<code1> "Select one Sound and one TextGrid"
<code2> "##ortho tier#: name of the tier where the orthographic transcription can be found"
<code2> "##phono tier#: name of the tier where the manually checked phonetization can be found"
<code2> "Checkbox Overwrite ??????"

<code1> "Choose a language"
<code2> "##language#: #fra French, #nan ??? #spa Spanish #slk Slovak #porbra Brasilian Portuguese"
<code2> "##Chars to ignore#: list the characters you want to ignore during the segmentation and alignment procedure"
<code2> "Checkbox Precise endpointing ???: 
My sentences (in ortho/phono) are delimited
1. loosely (with a single boundary in the middle of the pause) - recommended after automatic macro-segmentation (step 1.2)
2. precisely (with two boundaries around the pause) - recommended if step 3 has already been ran previously"

<code1> "Variation detection"
<code2> "##Checkbox Consider star#: use this option if you have not manually checked the automatic phonetization for liaisons"
<code2> "##Checkbox Allow elision#: use this option if you have not manually checked the automatic phonetization for schwa deletion"

<code1> "After processing," 
<code2> "Checkbox: Pre PTK thresholde: insert the duration (in ms) of the preocclusive silence"
<code2> "##Checkbox: Make syllable tier#: use this option for creating the $syll tier"
<code2> "##Checkbox: Open Sound and Textgrid: use this option in order to open Sound and Textgrid after processing, for manual check."

<entry> "3.2. After processing"
<normal> "Three tiers are added to the TextGrid : phones, syll, words. Verify that all utterances have been aligned. For this, one could either have a quick look at the Sound and TextGrid opened together and look for unaligned utterances, or have a look at the Info window which prints out a summary of the results of step 3. Several causes may explain unaligned utterances. Correct the TextGrid according the various cases described below then launch script 3. Segmentation again."

<list_item> "\bu ##Non-SAMPA characters in the phonetic transcription#: the transcription of phono tier has some characters that are not belonging to the SAMPA alphabet. To correct this, modify manually the phono tier or rerun step 3. Beware that if the step 2.Phonetisation is run again, manual modifications in the phono tier will be overwritten."

<list_item> "\bu ##The number of words is different in phono and ortho tiers#: during the manual checking of the phonetic transcription, one or more space characters might have been inserted or deleted. Correct this in ortho and/or phono tier. If the error comes from the automatic grapheme-to-phoneme conversion (step 2), correct it and report to the author (jeanphilippegoldman at gmail.com)"

<list_item> "\bu ##The segmentation could not be done# either because the utterance is too long or because the phonetic transcription is too distant from the pronunciation or because the signal quality is too bas or because the speech rate is too fast. Here are some ways to solve it: (1) Check the orthographic and phonetic transcriptions of unaligned utterances. Beware not to relaunch at this stage the script #2.Phonetisation, or manual modifications of phonetic transcription would be overwritten.
- Divide unaligned utterances into two or more utterances by inserting boundaries in phono and ortho tiers. Make sure that boundaries do coincide."

